EXEC		:= Main
CFLAGS		:= -Os -std=c++0x 
LDFLAGS		:= -Wall -pthread -lprussdrv -lrt -g -Wextra
CPP_FILES 	:= $(wildcard *.cpp)
CC              := g++

OBJS = $(subst .cpp,.o,$(CPP_FILES))

%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CC) -o $(EXEC) $(OBJS) $(LDFLAGS)

clean:
	-rm -f $(EXEC) *.gdb *.o
