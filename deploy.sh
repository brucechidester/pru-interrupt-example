#! /bin/bash

echo "-Building project"
   cd PRU0
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: PRU0 did not build!"
      exit
   fi

   cd ../PRU1
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: PRU1 did not build!"
      exit
   fi

echo "-Building Main"
   cd ..
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: Main did not build!"
      exit
   fi
   echo "Running application waiting for interrupt from PRU0:"
   time ./Main ./PRU0/PRU0.bin
   echo "Running application waiting for interrupt from PRU1:"
   time ./Main ./PRU1/PRU1.bin

